#!/bin/env python3
import pulsectl
import subprocess

def get_sources():
    with pulsectl.Pulse('source') as pulse:
        sources = pulse.source_list()
        return sources

def get_sinks():
    with pulsectl.Pulse('sink') as pulse:
        sinks = pulse.sink_list()
        return sinks

def get_default_sink():
    with pulsectl.Pulse('sink') as pulse:
        default_sink = pulse.server_info().default_sink_name
        return default_sink

def get_default_source():
    with pulsectl.Pulse('source') as pulse:
        default_source = pulse.server_info().default_source_name
        return default_source


def main():
    sources = get_sources()
    sinks = get_sinks()
    default_sink = get_default_sink()
    default_source = get_default_source()

    sources_str = '\n'.join([f'{source.description} {"*" if source.name == default_source else ""}' for source in sources])
    sinks_str = '\n'.join([f'{sink.description} {"*" if sink.name == default_sink else ""}' for sink in sinks])

    dmenu_input = f'Audio sources\n{sources_str}\n\nAudio sinks\n{sinks_str}'

    dmenu = subprocess.Popen(['dmenu', '-l', '20', '-p', 'Pulseaudio'], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    dmenu_output = dmenu.communicate(input=dmenu_input.encode())[0].decode().strip()

    sources_list = [source.description for source in sources]
    sinks_list = [sink.description for sink in sinks]
    if dmenu_output in sources_list:
        with pulsectl.Pulse('source') as pulse:
            source = pulse.source_list()[[source.description for source in sources].index(dmenu_output)]
            pulse.default_set(source)
    elif dmenu_output in sinks_list:
        with pulsectl.Pulse('sink') as pulse:
            sink = pulse.sink_list()[[sink.description for sink in sinks].index(dmenu_output)]
            pulse.default_set(sink)
    else:
        print('No source or sink selected')


if __name__ == '__main__':
    main()

